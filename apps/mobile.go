package apps

import (
  "../core"

	"fmt"
	"html/template"
	"net/http"
	"path"
  "io/ioutil"
)

type page struct {
	Title string
	Body  string
}

type Mobile struct {
  files map[string]string
}

func (m *Mobile) Create() error {
  ff, err := ioutil.ReadDir(core.Res())
  if err != nil {
    return err
  }
  
  m.files = make(map[string]string)
  
  for _, f := range ff {
    name := f.Name()
    fname := core.Res() + "/" + name
    m.files[name] =fname
  }
  return nil
}

func (m *Mobile) Close() error {
  return nil
}

func (m *Mobile) Handler(w http.ResponseWriter, r *http.Request) {
	p := &page{Title: "title", Body: "test"}

	_, ff := path.Split(r.URL.Path)

  name := "mobile_" + ff + ".html"
	file, t := m.files[name]
  if !t {
		file = core.Res() + "/mobile.html"
  }

	tmpl, err := template.ParseFiles(file)
	if err != nil {
		fmt.Fprintln(w, err)
		return
	}
	tmpl.Execute(w, p)
}

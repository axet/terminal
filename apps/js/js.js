core = {
  click : function(b, f) {
    b.disabled = true
    setTimeout(function() {
      f()
      b.disabled = false
    }, 500)
  },
  
  es : null,

  es_id : null,

  es_init: function() {
    if (core.es_id != null) {
      return core.es_id
    }

    var request = new XMLHttpRequest();
    request.onreadystatechange = function() {
      if (request.readyState == 4 && request.status == 200) {
        core.es_id = request.responseText.trim();
      }
    }
    request.open("GET", "/js/events", false);
    request.send(null);

    core.es = new EventSource("/js/events/" + core.es_id);
    core.es.onmessage = function(e) {
      var html = e.data.trim()
      var packet = JSON.parse(html)
      var listener = core.listener_map[packet.object_id]
      var args = packet["args"]
      var method = object["method"]
      listener[method].apply(this, args)
    }
    return core.es_id
  },
  
  rpc: function(packet) {
    var html;
    var request = new XMLHttpRequest();
    request.onreadystatechange = function() {
      if (request.readyState == 4 && request.status == 200) {
        html = request.responseText;
      }
    }
    request.open("POST", "/js/rpc", false);
    request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    request.send(JSON.stringify(packet));
    
    try {
      if (html.trim() == "OK") {
        return true
      }
      return JSON.parse(html)
    } catch(err) {
      throw html
    }
  },

  app: function(name) {
    var packet = {
      core: {
        method: 'app',
        name: name
      }
    };

    return core.rpc(packet)
  },

  devices_uuid: function(name) {
    var packet = {
      core: {
        method: 'devices_uuid',
        name: name
      }
    };

    var r = core.rpc(packet)
    return devices[r.type](r.uuid)
  },

  devices_type: function(name) {
    var packet = {
      core: {
        method: 'devices_type',
        name: name
      }
    };

    return core.rpc(packet)
  },

  listener_next : 0,

  listener_map : {},

  listener: function(name) {
    var l = listeners[name]()
    l.id = core.listener_next++
    return l
  },

  service: function(name) {
    var packet = {
      core: {
        method: 'service',
        name: name
      }
    };

    core.rpc(packet)

    return services[name]
  }
}

var listeners = {
  "CashDeviceListener" : function() {
    return {
      bill : function(bill) {
      },
    }
  }
}

var apps = {
  "Js" : {
    go2Js : function(es_id, object_id, method, args) {
      var packet = {
        service: {
          es_id: core.es_init(),
          object: "Js",
          method: "Go2Js",
          args: [
            es_id,
            object_id,
            method,
            args,
          ],
        }
      };

      return core.rpc(packet)
    },
    js2Go : function(ss, value) {
      var packet = {
        service: {
          es_id: core.es_init(),
          object: "Js",
          method: "Js2Go",
          args: [
            ss,
            value,
          ],
        }
      };

      return core.rpc(packet)
    },
    visit : function(path, f, err) {
      var packet = {
        service: {
          es_id: core.es_init(),
          object: "Js",
          method: "visit",
          args: [
            path,
            f,
            err,
          ],
        }
      };

      return core.rpc(packet)
    },
    create : function() {
      var packet = {
        service: {
          es_id: core.es_init(),
          object: "Js",
          method: "Create",
          args: [
          ],
        }
      };

      return core.rpc(packet)
    },
    close : function() {
      var packet = {
        service: {
          es_id: core.es_init(),
          object: "Js",
          method: "Close",
          args: [
          ],
        }
      };

      return core.rpc(packet)
    },
    handler : function(w, r) {
      var packet = {
        service: {
          es_id: core.es_init(),
          object: "Js",
          method: "Handler",
          args: [
            w,
            r,
          ],
        }
      };

      return core.rpc(packet)
    },
  },
  "Mobile" : {
    create : function() {
      var packet = {
        service: {
          es_id: core.es_init(),
          object: "Mobile",
          method: "Create",
          args: [
          ],
        }
      };

      return core.rpc(packet)
    },
    close : function() {
      var packet = {
        service: {
          es_id: core.es_init(),
          object: "Mobile",
          method: "Close",
          args: [
          ],
        }
      };

      return core.rpc(packet)
    },
    handler : function(w, r) {
      var packet = {
        service: {
          es_id: core.es_init(),
          object: "Mobile",
          method: "Handler",
          args: [
            w,
            r,
          ],
        }
      };

      return core.rpc(packet)
    },
  },
  "TestOpenGL" : {
    handler : function(w, r) {
      var packet = {
        service: {
          es_id: core.es_init(),
          object: "TestOpenGL",
          method: "Handler",
          args: [
            w,
            r,
          ],
        }
      };

      return core.rpc(packet)
    },
    create : function() {
      var packet = {
        service: {
          es_id: core.es_init(),
          object: "TestOpenGL",
          method: "Create",
          args: [
          ],
        }
      };

      return core.rpc(packet)
    },
    close : function() {
      var packet = {
        service: {
          es_id: core.es_init(),
          object: "TestOpenGL",
          method: "Close",
          args: [
          ],
        }
      };

      return core.rpc(packet)
    },
  }
}

var services = {
  "Hardware" : {
    enumeratePorts : function() {
      var packet = {
        service: {
          es_id: core.es_init(),
          object: "Hardware",
          method: "EnumeratePorts",
          args: [
          ],
        }
      };

      return core.rpc(packet)
    },
    probe : function() {
      var packet = {
        service: {
          es_id: core.es_init(),
          object: "Hardware",
          method: "Probe",
          args: [
          ],
        }
      };

      return core.rpc(packet)
    },
    create : function() {
      var packet = {
        service: {
          es_id: core.es_init(),
          object: "Hardware",
          method: "Create",
          args: [
          ],
        }
      };

      return core.rpc(packet)
    },
    run : function() {
      var packet = {
        service: {
          es_id: core.es_init(),
          object: "Hardware",
          method: "Run",
          args: [
          ],
        }
      };

      return core.rpc(packet)
    },
    close : function() {
      var packet = {
        service: {
          es_id: core.es_init(),
          object: "Hardware",
          method: "Close",
          args: [
          ],
        }
      };

      return core.rpc(packet)
    },
  },
  "Money" : {
    create : function() {
      var packet = {
        service: {
          es_id: core.es_init(),
          object: "Money",
          method: "Create",
          args: [
          ],
        }
      };

      return core.rpc(packet)
    },
    close : function() {
      var packet = {
        service: {
          es_id: core.es_init(),
          object: "Money",
          method: "Close",
          args: [
          ],
        }
      };

      return core.rpc(packet)
    },
    bill : function(bill) {
      var packet = {
        service: {
          es_id: core.es_init(),
          object: "Money",
          method: "Bill",
          args: [
            bill,
          ],
        }
      };

      return core.rpc(packet)
    },
    run : function() {
      var packet = {
        service: {
          es_id: core.es_init(),
          object: "Money",
          method: "Run",
          args: [
          ],
        }
      };

      return core.rpc(packet)
    },
    clear : function() {
      var packet = {
        service: {
          es_id: core.es_init(),
          object: "Money",
          method: "Clear",
          args: [
          ],
        }
      };

      return core.rpc(packet)
    },
    getAmount : function() {
      var packet = {
        service: {
          es_id: core.es_init(),
          object: "Money",
          method: "GetAmount",
          args: [
          ],
        }
      };

      return core.rpc(packet)
    },
  },
  "Qiwi" : {
    create : function() {
      var packet = {
        service: {
          es_id: core.es_init(),
          object: "Qiwi",
          method: "Create",
          args: [
          ],
        }
      };

      return core.rpc(packet)
    },
    close : function() {
      var packet = {
        service: {
          es_id: core.es_init(),
          object: "Qiwi",
          method: "Close",
          args: [
          ],
        }
      };

      return core.rpc(packet)
    },
    run : function() {
      var packet = {
        service: {
          es_id: core.es_init(),
          object: "Qiwi",
          method: "Run",
          args: [
          ],
        }
      };

      return core.rpc(packet)
    },
    payment : function(provider, amount) {
      var packet = {
        service: {
          es_id: core.es_init(),
          object: "Qiwi",
          method: "Payment",
          args: [
            provider,
            amount,
          ],
        }
      };

      return core.rpc(packet)
    },
    payment2 : function(provider, amount) {
      var packet = {
        service: {
          es_id: core.es_init(),
          object: "Qiwi",
          method: "Payment2",
          args: [
            provider,
            amount,
          ],
        }
      };

      return core.rpc(packet)
    },
  },
  "Restart" : {
    run : function() {
      var packet = {
        service: {
          es_id: core.es_init(),
          object: "Restart",
          method: "Run",
          args: [
          ],
        }
      };

      return core.rpc(packet)
    },
    create : function() {
      var packet = {
        service: {
          es_id: core.es_init(),
          object: "Restart",
          method: "Create",
          args: [
          ],
        }
      };

      return core.rpc(packet)
    },
    close : function() {
      var packet = {
        service: {
          es_id: core.es_init(),
          object: "Restart",
          method: "Close",
          args: [
          ],
        }
      };

      return core.rpc(packet)
    },
  },
  "Update" : {
    create : function() {
      var packet = {
        service: {
          es_id: core.es_init(),
          object: "Update",
          method: "Create",
          args: [
          ],
        }
      };

      return core.rpc(packet)
    },
    run : function() {
      var packet = {
        service: {
          es_id: core.es_init(),
          object: "Update",
          method: "Run",
          args: [
          ],
        }
      };

      return core.rpc(packet)
    },
    close : function() {
      var packet = {
        service: {
          es_id: core.es_init(),
          object: "Update",
          method: "Close",
          args: [
          ],
        }
      };

      return core.rpc(packet)
    },
  },
  "Web" : {
    handler : function(w, r) {
      var packet = {
        service: {
          es_id: core.es_init(),
          object: "Web",
          method: "Handler",
          args: [
            w,
            r,
          ],
        }
      };

      return core.rpc(packet)
    },
    create : function() {
      var packet = {
        service: {
          es_id: core.es_init(),
          object: "Web",
          method: "Create",
          args: [
          ],
        }
      };

      return core.rpc(packet)
    },
    run : function() {
      var packet = {
        service: {
          es_id: core.es_init(),
          object: "Web",
          method: "Run",
          args: [
          ],
        }
      };

      return core.rpc(packet)
    },
    close : function() {
      var packet = {
        service: {
          es_id: core.es_init(),
          object: "Web",
          method: "Close",
          args: [
          ],
        }
      };

      return core.rpc(packet)
    },
  }
}

var devices = {
  "BillValidatorCCNET" : function(uuid) {
    return {
      create : function() {
        var packet = {
          device: {
            es_id: core.es_init(),
            object: uuid,
            method: "Create",
            args: [
            ],
          }
        };
        return core.rpc(packet)
      },
      close : function() {
        var packet = {
          device: {
            es_id: core.es_init(),
            object: uuid,
            method: "Close",
            args: [
            ],
          }
        };
        return core.rpc(packet)
      },
      id : function() {
        var packet = {
          device: {
            es_id: core.es_init(),
            object: uuid,
            method: "Id",
            args: [
            ],
          }
        };
        return core.rpc(packet)
      },
      run : function() {
        var packet = {
          device: {
            es_id: core.es_init(),
            object: uuid,
            method: "Run",
            args: [
            ],
          }
        };
        return core.rpc(packet)
      },
      open : function(port) {
        var packet = {
          device: {
            es_id: core.es_init(),
            object: uuid,
            method: "Open",
            args: [
            port,
            ],
          }
        };
        return core.rpc(packet)
      },
      addListener : function(l) {
        var packet = {
          device: {
            es_id: core.es_init(),
            object: uuid,
            method: "AddListener",
            args: [
            l.id,
            ],
          }
        };
        core.listener_map[l.id] = l
        return core.rpc(packet)
      },
      removeListener : function(l) {
        var packet = {
          device: {
            es_id: core.es_init(),
            object: uuid,
            method: "RemoveListener",
            args: [
            l.id,
            ],
          }
        };
        delete core.listener_map[l.id]
        return core.rpc(packet)
      },
      process : function() {
        var packet = {
          device: {
            es_id: core.es_init(),
            object: uuid,
            method: "Process",
            args: [
            ],
          }
        };
        return core.rpc(packet)
      },
      bill2Buf : function(bills) {
        var packet = {
          device: {
            es_id: core.es_init(),
            object: uuid,
            method: "Bill2Buf",
            args: [
            bills,
            ],
          }
        };
        return core.rpc(packet)
      },
      buf2Bill : function(buf) {
        var packet = {
          device: {
            es_id: core.es_init(),
            object: uuid,
            method: "Buf2Bill",
            args: [
            buf,
            ],
          }
        };
        return core.rpc(packet)
      },
      writeCmd : function(cmd) {
        var packet = {
          device: {
            es_id: core.es_init(),
            object: uuid,
            method: "WriteCmd",
            args: [
            cmd,
            ],
          }
        };
        return core.rpc(packet)
      },
      writeData : function(cmd, data) {
        var packet = {
          device: {
            es_id: core.es_init(),
            object: uuid,
            method: "WriteData",
            args: [
            cmd,
            data,
            ],
          }
        };
        return core.rpc(packet)
      },
      readData : function() {
        var packet = {
          device: {
            es_id: core.es_init(),
            object: uuid,
            method: "ReadData",
            args: [
            ],
          }
        };
        return core.rpc(packet)
      },
      readCmd : function() {
        var packet = {
          device: {
            es_id: core.es_init(),
            object: uuid,
            method: "ReadCmd",
            args: [
            ],
          }
        };
        return core.rpc(packet)
      },
      reset : function() {
        var packet = {
          device: {
            es_id: core.es_init(),
            object: uuid,
            method: "Reset",
            args: [
            ],
          }
        };
        return core.rpc(packet)
      },
      setSecurity : function(highsecuritybills) {
        var packet = {
          device: {
            es_id: core.es_init(),
            object: uuid,
            method: "SetSecurity",
            args: [
            highsecuritybills,
            ],
          }
        };
        return core.rpc(packet)
      },
      powerRecovery : function() {
        var packet = {
          device: {
            es_id: core.es_init(),
            object: uuid,
            method: "PowerRecovery",
            args: [
            ],
          }
        };
        return core.rpc(packet)
      },
      enableBillTypesAll : function() {
        var packet = {
          device: {
            es_id: core.es_init(),
            object: uuid,
            method: "EnableBillTypesAll",
            args: [
            ],
          }
        };
        return core.rpc(packet)
      },
      enableBillTypes : function(enabled, escrow) {
        var packet = {
          device: {
            es_id: core.es_init(),
            object: uuid,
            method: "EnableBillTypes",
            args: [
            enabled,
            escrow,
            ],
          }
        };
        return core.rpc(packet)
      },
      stack : function() {
        var packet = {
          device: {
            es_id: core.es_init(),
            object: uuid,
            method: "Stack",
            args: [
            ],
          }
        };
        return core.rpc(packet)
      },
      cassetteStatus : function() {
        var packet = {
          device: {
            es_id: core.es_init(),
            object: uuid,
            method: "CassetteStatus",
            args: [
            ],
          }
        };
        return core.rpc(packet)
      },
      return : function() {
        var packet = {
          device: {
            es_id: core.es_init(),
            object: uuid,
            method: "Return",
            args: [
            ],
          }
        };
        return core.rpc(packet)
      },
      dispense : function(bills) {
        var packet = {
          device: {
            es_id: core.es_init(),
            object: uuid,
            method: "Dispense",
            args: [
            bills,
            ],
          }
        };
        return core.rpc(packet)
      },
      unload : function(cassette, bills) {
        var packet = {
          device: {
            es_id: core.es_init(),
            object: uuid,
            method: "Unload",
            args: [
            cassette,
            bills,
            ],
          }
        };
        return core.rpc(packet)
      },
      setCassetteType : function() {
        var packet = {
          device: {
            es_id: core.es_init(),
            object: uuid,
            method: "SetCassetteType",
            args: [
            ],
          }
        };
        return core.rpc(packet)
      },
      setOptions : function(opts) {
        var packet = {
          device: {
            es_id: core.es_init(),
            object: uuid,
            method: "SetOptions",
            args: [
            opts,
            ],
          }
        };
        return core.rpc(packet)
      },
      emptyDispenser : function() {
        var packet = {
          device: {
            es_id: core.es_init(),
            object: uuid,
            method: "EmptyDispenser",
            args: [
            ],
          }
        };
        return core.rpc(packet)
      },
      poll : function() {
        var packet = {
          device: {
            es_id: core.es_init(),
            object: uuid,
            method: "Poll",
            args: [
            ],
          }
        };
        return core.rpc(packet)
      },
      getAck : function() {
        var packet = {
          device: {
            es_id: core.es_init(),
            object: uuid,
            method: "GetAck",
            args: [
            ],
          }
        };
        return core.rpc(packet)
      },
      getStatus : function() {
        var packet = {
          device: {
            es_id: core.es_init(),
            object: uuid,
            method: "GetStatus",
            args: [
            ],
          }
        };
        return core.rpc(packet)
      },
      getBillTable : function() {
        var packet = {
          device: {
            es_id: core.es_init(),
            object: uuid,
            method: "GetBillTable",
            args: [
            ],
          }
        };
        return core.rpc(packet)
      },
      getIdentification : function() {
        var packet = {
          device: {
            es_id: core.es_init(),
            object: uuid,
            method: "GetIdentification",
            args: [
            ],
          }
        };
        return core.rpc(packet)
      },
      getExtendedIdentification : function() {
        var packet = {
          device: {
            es_id: core.es_init(),
            object: uuid,
            method: "GetExtendedIdentification",
            args: [
            ],
          }
        };
        return core.rpc(packet)
      }
    }
  }
}

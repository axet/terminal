package apps

import (
	"fmt"
	"net/http"
)

type Alpha struct {
}

func (m *Alpha) Handler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Hi there, I love Alpha %s!", r.URL.Path[1:])
}

func (m *Alpha) Create() error {
  return nil
}

func (m *Alpha) Close() error {
  return nil
}

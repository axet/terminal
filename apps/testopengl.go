package apps

import (
	"fmt"
	"net/http"
)

type TestOpenGL struct {
}

func (m *TestOpenGL) Handler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Hi there, I love TestOpenGL %s!", r.URL.Path[1:])
}

func (m *TestOpenGL) Create() error {
  return nil
}

func (m *TestOpenGL) Close() error {
  return nil
}

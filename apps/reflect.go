package apps

import "reflect"
import "../core"

func init() {
  core.AppsMap = map[string]reflect.Type {
    "Js": reflect.TypeOf((*Js)(nil)).Elem(),
    "Mobile": reflect.TypeOf((*Mobile)(nil)).Elem(),
    "TestOpenGL": reflect.TypeOf((*TestOpenGL)(nil)).Elem(),
  }
}

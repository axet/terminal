package apps

import (
  "reflect"
  "../core"
)

// Name -> Reflect Type map
var JsListenersMap map[string]reflect.Type
// Channel + ID -> Object map
var JsListeners = make(map[string]interface{})

func init() {
  JsListenersMap = map[string]reflect.Type {

    "CashDeviceListener": reflect.TypeOf((*CashDeviceListenerPrototype)(nil)).Elem(),
  }
}

type CashDeviceListenerPrototype struct {
  js *Js
  // javascript events channel id
  es_id string
  // javascript listener object id
  object_id int
}

func (m *CashDeviceListenerPrototype) Create(js interface{}, es_id string, object_id int) {
  m.js = js.(*Js)
  m.es_id = es_id
  m.object_id = object_id
}

func (m *CashDeviceListenerPrototype) Bill(bill core.Bill) {
  m.js.Go2Js(m.es_id, m.object_id, "bill", bill)
}

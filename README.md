# terminal

Street kiosk / venging machine opensource model.

Cross device support, HTML5 based, simple architecture. This project compile from source and can work on any hardware from home router up to banking vending machine.

![terminal](/docs/terminal.png)

Go source code can be compile on various hardware including ARM/x86/MIPS and other machines and give no limitations for hardware or OS using.

It can work with HTML5/OpenGL or be simple service / vending machine headless solution.

You can choice to use HTML5/JavaScript only without digging into Go source to write your own applications.

You can build cheap solution: home router + external monitor + external USB cashvalidator ant manage to accept payments. Or build full feature platform for custom banking solution.

![cost](/docs/cost.png)

# Types

  * apps - Applications. Basic primitive, which implements all logic. Want to add new vending feature which rendered as HTML5 and appears as a button on main screen? Write an application!
  * devices - Devices. have an external device? cachacceptor? coinacceptor? Use available drivers or write your own!
  * services - Services. have background tasks for scheduling synching or secure storage use available services

# Control flow

Go platform starts and run all services enabled in platform. Simple explanation:

  * ```main.go -> core.go -> services/*.go```
  * ```services/web.go -> apps/*.go```
  * ```apps/js.go -> apps/js/*.html```

Go source code compiles into binary file. Binary file runs main.go which runs core.go.

core.go search for services enabled. Services can be enumerated from config file or enabled services can be downloaded from central server.

Services starts. One of service responsable to run local http server, it called `web.go`. After start it read allowed applications list and register them as a http handler.

All enabled application will get local urls like: http://localhost/mobile/1

where 'localhost' - local server. /mobile/ - mobile.go application file responsable to show mobile operators payment process, accept money and print tickets. /1/ - application encoded parameters can mean mobile operator code.

# Calls

Go platform allows you to call Go code from javascript. Platform allows you to code using clean javascript without any browser modification nessesery.

``` javascript
<script>
    // add device listener example
    var dd = core.devices_type("CashDevice")
    dd.forEach(function(d) {
      var l = core.listener("CashDeviceListener")
      l.bill = function(bill) {
        console.log("html listener " + bill.Value + " " + bill.Currency);
      }
      d.addListener(l)
    });

    // payment service call example
    var q = core.service("Qiwi")
    var t = q.payment("mobile/megafon", 100)
    console.log(t)
</script>
```

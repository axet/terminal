package services

import (
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"strconv"
	"syscall"
	"time"
)

type Restart struct {
}

func (this *Restart) Run() error {
	file, err := filepath.Abs(os.Args[0])
	if err != nil {
		return err
	}

	info, err := os.Stat(file)
	if err != nil {
		return err
	}

	for {
		info2, err := os.Stat(file)
		if err != nil {
			return err
		}

		if !info.ModTime().Equal(info2.ModTime()) {
			log.Println("Restarting...")
			pid := strconv.Itoa(os.Getpid())
			c := exec.Command(file, "-wait", pid)
			c.Stdout = os.Stdout
			c.Stderr = os.Stderr
			c.SysProcAttr = &syscall.SysProcAttr{Setpgid: true}
			err := c.Start()
			if err != nil {
				log.Println(err)
			} else {
				os.Exit(0)
			}
		}

		time.Sleep(1 * time.Second)
	}
	return nil
}

func (m *Restart) Create() error {
  return nil
}

func (this *Restart) Close() error {
	return nil
}

package services

import (
  "../core"
  "reflect"
  "sync"
)

type Hardware struct {
  wg sync.WaitGroup
}

func (m *Hardware) EnumeratePorts() ([]string, error) {
  return []string{"/dev/tty.usbserial"}, nil
}

func (m *Hardware) Probe() (map[string]interface{}, error) {
  devices := make(map[string]interface{})

  ports, err := m.EnumeratePorts()
  if err != nil {
    return nil, err
  }

	for k, v := range core.DevicesMap {
		s, t := reflect.New(v).Interface().(core.Device)
    if !t {
      core.Err("Error: wrong interface", k)
      continue
    }
    err := s.Create()
    if err !=nil {
      core.Err("Error:", err)
      continue
    }
    c, t := s.(core.COMDevice)
    if t {
      for _, port := range ports {
        err := c.Open(port)
        if err == nil {
          uuid, err := core.UUID()
          if err != nil {
            return nil, err
          }
          devices[uuid] = core.COMDeviceConfig{core.DeviceConfig{UUID:uuid, Id:s.Id(), Type:reflect.TypeOf(c).Elem().Name()}, port}
        }
        s.Close()
      }
    }
  }

  return devices, nil
}

func (m *Hardware) Create() error {
  // get devices on terminal by reading config file, or by dynamically probing available.
  dd, err := m.Probe()
  if err != nil {
    return err
  }

  for k, v := range dd {
    device := v.(core.DeviceConfig)
		s, t := reflect.New(core.DevicesMap[device.Type]).Interface().(core.Device)
    if !t {
      core.Err("Error: wrong interface", k)
      continue
    }
    err := s.Create()
    if err !=nil {
      core.Err("Error:", err)
      continue
    }
    c, t := s.(core.COMDevice)
    if t {
      vv := v.(core.COMDeviceConfig)
      err := c.Open(vv.Port)
      if err != nil {
        core.Err("unable open com device", err)
        continue
      }
    }
    core.Devices[device.UUID] = s

    m.wg.Add(1)
    go func() {
      defer m.wg.Done()
      err := s.Run()
      if err != nil {
        core.Err("Device", device.UUID, device.Type, err)
      }
    }()
	}

  return nil
}

func (m *Hardware) Run() error {
  m.wg.Wait()
  return nil
}

func (m *Hardware) Close() error {
	return nil
}

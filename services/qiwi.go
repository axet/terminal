package services

import (
	"log"
)

type Qiwi struct {
}

func (i *Qiwi) Create() error {
	return nil
}

func (i *Qiwi) Close() error {
	return nil
}

func (m *Qiwi) Run() error {
  return nil
}

func (i *Qiwi) Payment(provider string, amount int) {
	log.Println("qiwi payment", provider, amount)
}

func (i *Qiwi) Payment2(provider string, amount int) (int, string, float64) {
	log.Println("qiwi payment2", provider, amount)
  return 10, "abc", 1.1
}

package services

import (
	"../core"
	"fmt"
	"html/template"
	"net/http"
	"reflect"
	"strings"
)

type Web struct {
	// enabled providers
	providers []string
}

type page struct {
	Title string
	Body  string
}

func (web *Web) Handler(w http.ResponseWriter, r *http.Request) {
	p := &page{Title: "title", Body: "test"}

	page := ""

	if strings.HasSuffix(r.URL.Path, "2") {
		page = " 2"
	}

	if strings.HasSuffix(r.URL.Path, "3") {
		page = " 3"
	}

	file := core.Res() + "/index" + page + ".html"

	t, err := template.ParseFiles(file)
	if err != nil {
		fmt.Fprintln(w, err)
		return
	}
	t.Execute(w, p)
}

func (web *Web) Create() error {
	// can be loaded from a config file, or downloaded from internet.
  // then parsed, app name extracted and handled by http with allowed urls.
  // those urls will be used to generate index.html for main page.
	web.providers = []string{"mobile/1", "mobile/2", "mobile/mts", "mobile/megafon"}

	for key, app := range core.AppsMap {
		i := reflect.New(app).Interface()
		a, b := i.(core.App)
		if !b {
			core.Err("Error: bad app ", key)
			continue
		}
		err := a.Create()
    if err != nil {
      core.Err("app error", err)
      continue
    }
		http.HandleFunc(strings.ToLower("/"+key+"/"), a.Handler)

		core.Apps[key] = a
	}

	http.HandleFunc("/", web.Handler)
  return nil
}

func (m *Web) Run() error {
	return http.ListenAndServe(":8080", nil)
}

func (web *Web) Close() error {
	return nil
}

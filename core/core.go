package core

import (
	"reflect"
  "log"
  "sync"
  "errors"
	"crypto/rand"
	"fmt"
	"io"
  "runtime"
  "path/filepath"
  "strings"
)

// Name -> Reflect Type map
var AppsMap map[string]reflect.Type
// Name -> Reflect Type map
var ServicesMap map[string]reflect.Type
// Name -> Reflect Type map
var DevicesMap map[string]reflect.Type
// Name -> Reflect Type map
var TypesMap map[string]reflect.Type

// Name -> Object map
var Apps = make(map[string]interface{})
// Name -> Object map
var Services = make(map[string]interface{})
// UUID -> Object map
var Devices = make(map[string]interface{})

var wg sync.WaitGroup

//
// non fatal error. error reporting from Go routines. keep running.
//

func Err(err ...interface{}) {
  log.Println(err...)
}

//
// Service depends function
//

func Depends(names ...string) error {
  for _, name := range names {
    _, t := Services[name]
    if t {
      // skip if service running already
      continue
    }
    n, t := ServicesMap[name]
    if !t {
      // exit if service does not exist
      return errors.New("unknown service: " + name)
    }
  	s, t := reflect.New(n).Interface().(Service)
    if !t {
      return errors.New("Error: wrong interface " + name)
    }
    err := s.Create()
    if err != nil {
      return errors.New("Error: " + err.Error())
    }
    Services[name] = s

    wg.Add(1)
    go func() {
      defer wg.Done()
      err := s.Run()
      if err != nil {
        Err("Service", name, err)
      }
    }()
  }
  return nil
}

func Create() error {
	for k, _ := range ServicesMap {
    err := Depends(k)
    if err != nil {
      return err
    }
	}
  return nil
}

func Run() error {
  wg.Wait()
  return nil
}

func Close() error {
	for _, v := range Services {
    err := v.(Service).Close()
    if err != nil {
      return err
    }
	}
  return nil
}

func UUID() (string, error) {
	uuid := make([]byte, 16)
	n, err := io.ReadFull(rand.Reader, uuid)
	if n != len(uuid) || err != nil {
		return "", err
	}
	// variant bits; see section 4.1.1
	uuid[8] = uuid[8]&^0xc0 | 0x80
	// version 4 (pseudo-random); see section 4.1.3
	uuid[6] = uuid[6]&^0xf0 | 0x40
	return fmt.Sprintf("%x-%x-%x-%x-%x", uuid[0:4], uuid[4:6], uuid[6:8], uuid[8:10], uuid[10:]), nil
}

//
// get resources path
//
// for ./apps/js.go it would be apps/js, ./services/web.go --> services/web
//
func Res() string {
  _, file, _, ok := runtime.Caller(1)
  if !ok {
    panic("unable to get Caller info")
  }
  pp := strings.Split(file, "/")
  // take two last elements, if first is known element (app, service, device)
  // then basename for last and return a path.
  //
  // if first is not known, then return last basename as a path
  pp = pp[len(pp)-2:]
  pp[1] = strings.TrimSuffix(pp[1], filepath.Ext(pp[1]))
  switch pp[0] {
  case "core", "services", "apps", "devices":
    return filepath.Join(pp...)
  default:
    return pp[1]
  }
}

package core

//
// money related structures.
//
// all devices may have its own money types / formats. we have to unite it all as our type. some devices may say "USA" on
// banknotes which are USD iso codes. or have 2 country codes or so... so we need to convert all devices / software
// data to meet our standarts.
//

import (
  "fmt"
)

type Bill struct {
	// Value in cents, 1 USD ==> (Value == 100 cents)
	Value int
	// Currency ISO_4217 standard, "USD", "EUR", "RUB"
	Currency string
}

func (m *Bill) String() string {
  if m.Value < 100 {
    return fmt.Sprint(m.Value, "Cents", m.Currency)
  }else {
    return fmt.Sprint(m.Value / 100, "Bills", m.Currency)
  }
}

const EUR = "EUR"
const RUB = "RUB"
const USD = "USD"

var BILL50EUR = Bill{Value: 5000, Currency: EUR}
var BILL10EUR = Bill{Value: 1000, Currency: EUR}

var BILL50RUB = Bill{Value: 5000, Currency: RUB}
var BILL10RUB = Bill{Value: 1000, Currency: RUB}
